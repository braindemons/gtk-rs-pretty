use std::{cell::Cell, env::args, path::PathBuf};

use {
    gdk::Screen,
    gdk_pixbuf::Pixbuf,
    gio::{prelude::*, ApplicationFlags},
    gtk::{
        prelude::*, AboutDialog, Application, ApplicationWindow, Button, CssProvider, IconTheme,
        Label, Orientation, Settings, SettingsExt, StyleContext, WindowPosition, Window,
    },
};

fn main() {
    let application = gtk::Application::new(
        Some("com.braindemons.examples.gtk-rs-pretty"),
        ApplicationFlags::FLAGS_NONE,
    )
    .unwrap();

    application.connect_activate(|app| {
        let screen = Screen::get_default().unwrap();
        let settings = Settings::get_for_screen(&screen).unwrap();
        let data_path = data_path();

        init_theme(&data_path, &screen, &settings);
        init_icons(&data_path, &settings);

        build_ui(app, data_path)
    });

    application.run(&args().collect::<Vec<_>>());
}

fn init_theme(data_path: &PathBuf, screen: &Screen, settings: &Settings) {
    // Load the application theme
    let css_provider = CssProvider::new();
    let mut css_path = data_path.clone();
    css_path.push("theme.css");
    css_provider
        .load_from_path(&css_path.to_string_lossy())
        .unwrap();

    // Apply the application theme
    StyleContext::add_provider_for_screen(
        &screen,
        &css_provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    // Forcefully set to the Adwaita theme just in case
    settings.set_property_gtk_theme_name(Some("Adwaita"));
}

fn init_icons(data_path: &PathBuf, settings: &Settings) {
    // Add the application's icons path
    let icon_theme = IconTheme::get_default().unwrap();
    icon_theme.append_search_path(&data_path);

    // Set to the included icon theme
    settings.set_property_gtk_icon_theme_name(Some("pretty-icons"));
}

fn data_path() -> PathBuf {
    if let Ok(cargo_dir) = std::env::var("CARGO_MANIFEST_DIR") {
        let mut data_path = PathBuf::from(cargo_dir);

        // If we're executed from cargo, use the cargo manifest directory
        data_path.push("data");

        // Append /dist to it, we've also got source files in the data directory that we want to
        // keep separate
        data_path.push("dist");

        data_path
    } else {
        // Otherwise, use the executable's directory
        let mut data_path = std::env::current_exe().unwrap().to_owned();
        data_path.pop();
        data_path.push("data");

        data_path
    }
}

fn build_ui(application: &Application, data_path: PathBuf) {
    // Load the logo
    let mut logo_path = data_path.clone();
    logo_path.push("logo.png");
    let logo = Pixbuf::new_from_file(&logo_path).unwrap();

    Window::set_default_icon(&logo);

    // Create and configure the window
    let window = ApplicationWindow::new(application);

    window.set_title("Pretty GTK");
    window.set_border_width(10);
    window.set_position(WindowPosition::Center);
    window.set_default_size(350, 150);

    let container = gtk::Box::new(Orientation::Vertical, 6);
    window.add(&container);

    let button = Button::new_with_label("Click me!");
    container.add(&button);

    let label = Label::new(Some(&label_text(0)));
    container.add(&label);

    let about_button = Button::new_with_label("About");
    container.pack_end(&about_button, false, false, 0);

    window.show_all();

    // Increment the count on button click
    let count = Cell::new(0);
    button.connect_clicked(move |_| {
        let mut value = count.get();
        value += 1;
        count.set(value);
        label.set_label(&label_text(value));
    });

    // Open an about window on about click
    about_button.connect_clicked(move |_| {
        let p = AboutDialog::new();

        p.set_title("About");

        p.set_comments(Some(
            "A simple example on how to make your GTK applications look nice out of the box.",
        ));
        p.set_authors(&["BrainDemons"]);
        p.set_license(Some("MIT OR Apache-2"));
        p.set_transient_for(Some(&window));

        p.run();
        p.destroy();
    });
}

fn label_text(count: usize) -> String {
    format!("Clicked {} Times", count)
}