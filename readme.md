# GTK Rust Pretty

A simple example on how to make your GTK applications look nice out of the box.

![Screenshot](screenshot.png)


## Data

This application will search in different locations for assets, depending on how it's run.

- cargo run: `$CARGO_MANIFEST_DIR/data/dist`
- standalone: `$EXE_DIR/data`

Keep this in mind when publishing, you need to include the `data/dist` directory as `data`.


## Theme

The theme stylesheet is generated from `theme/theme.styl`.
A pre-compiled `data/dist/theme.css` is included.
To compile `theme.styl`, you need to install stylus globally through npm, and then run
`build-theme.ps1` or `build-theme.sh`.


## Windows

On Windows, GTK's binaries are pulled in using vcpkg.

```
vcpkg install gtk
```

To build, you need to enable the dynamic linking mode in vcpkg-rs.

```
$env:VCPKGRS_DYNAMIC=1
```


## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
