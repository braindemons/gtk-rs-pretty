fn main() {
    // On windows we use vcpkg, anywhere else we can rely on pkg-config in the dependencies

    if cfg!(windows) {
        vcpkg::find_package("gtk").unwrap();

        // Without this harfbuzz.dll isn't copied
        vcpkg::find_package("harfbuzz").unwrap();
    }
}